# Mascarade
Mascarade is a project by Jasper de Boer, Stefan Bussemaker and Margriet Vegter for the course Multi-Agent Systems at the University of Groningen.

<!-- TOC -->

- [Mascarade](#mascarade)
  - [The Game](#the-game)
  - [Implementation](#implementation)
  - [Development](#development)
    - [Setup](#setup)
    - [Running the project](#running-the-project)

<!-- /TOC -->

## The Game
Mascarade is a card game, developed by Bruno Faidutti in 2013. The game is about reasoning which cards you and your opponents have. Each player has one card (with some character, i.e. King, Queen or Judge) and the game is won by scoring some fixed number of coins. While most games start with all characters hidden, this game starts with all players having their card faced up, so that everyone knows who has which card. The cards are then flipped and the players can no longer see each other's cards. During the game players can perform three actions:

1. Swap their card - or not - with another player's card in a concealing manner, e.g. under the table. This introduces new uncertainty. Only the player who performed the action knows whether or not he switched the cards, the other players now hold both options for possible. 
2. Secretly look at their card. This can be a good choice when a lot of switching has taken place and the player is too uncertain of its own card. He now knows which card he has again.
3. Announce their character

The first four turns are used to introduce some uncertainty into the game. The first four players have to sequentially perform action 1. From the fifth turn on a player can also choose the second or third action. The announcement action can be performed when a player knows or bluffs to know their character. After an announcement each player has an opportunity to claim that they have the card of the announcer. If one or more other players claim to be the same character, all concerned players reveal their cards. The winning player gets a coin, while the falsely claiming parties lose one coin. After this the players turn over their cards again. If the next player revealed their card in the last turn, he has is limited to performing action 1. 

## Implementation
The game is modelled using Public Announcement Logic (PAL). The revealing of the cards is the announcing in our model. Every player then knows which player has which card; at least, those that are revealed, the other ones remain unknown. This also means that the players know that the other players do not have those cards, since each cards is unique and therefore no two players can have the same card. To keep our model simple, we start with 3 agents. The possible actions are kept the same. The game also features different character abilities, so that the players get rewards depending on the character that they currently have. However, since this unnecessarily increases the complexity of the game, this is omitted. 

## Development 
### Setup
The project is written in Typescript and uses the AngularJS web framework. To run this, Node.js is needed. It can be installed using `nvm` (installation instructions can be found [here](https://goo.gl/6gToKj)), after which Node.js is installed using `nvm install node --lts`. At this point you can clone the repo and run `npm install` to install all project dependencies. Now you're all set! The Visual Studio Code is the recommended editor, due to their strong TypeScript support. More information on Typescript can be found on https://www.typescriptlang.org/. The main script files can be found in `/src/app/`

### Running the project
Run the project using `npm run serve`. It serves the web page on `http://localhost:3000` by default. Logging can be inspected in the browser's debugging page, for Chrome this is the Chrome Debugger, which can be opened using Ctrl+Shift+J
