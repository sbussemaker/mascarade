import * as d3 from 'd3';
import * as _ from 'lodash';

import { IGameController } from './game';
import { Model, State, Relation } from './model';
import { Agent } from './agent';

export interface IGraph {
  update: () => void;
}

export class Graph implements IGraph {
  private svg: any;
  private width: number;
  private height: number;
  private node;
  private link;
  private linktext;
  private color;
  private simulation;
  private zoom = d3.zoom().scaleExtent([0.1, 7]);

  constructor(
    private nAgents: number,
    private game: IGameController,
    private model: Model
  ) {}

  watchRelations () {
    // call update if relations have changed
    this.game.$scope.$watch('$ctrl.relations', (newVal, oldVal) => {
      if (newVal === oldVal) { return; }
      console.log('update relations');
      this.update();
    });

    // call update if states have changed
    this.game.$scope.$watch('$ctrl.states', (newVal, oldVal) => {
      if (newVal === oldVal) { return; }
      console.log('update states');
      this.update();
    });
  }

  setupModelGraph () {
    this.svg = d3.select('div#graph')
      .append('div')
      .classed('svg-container', true) // container class to make it responsive
      .append('svg')
      // responsive SVG needs these 2 attributes and no width and height attr
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('viewBox', '-300 -200 600 400')
      // class to make it responsive
      .classed('svg-content-responsive', true);

    this.width = +this.svg.attr('width');
    this.height = +this.svg.attr('height');
    this.color = d3.scaleOrdinal(d3.schemeCategory20);

    this.simulation = d3.forceSimulation(this.model.states)
        .force('link', d3.forceLink()
          .id((d: any) => { return d.id; })
          .distance(150)
          .strength(0.1))
        .force('charge', d3.forceManyBody()
          .strength(-500))
        .force('center', d3.forceCenter(this.width / 2, this.height / 2))
        .alpha(0.3)
        .on('tick', this.ticked);

    this.link = this.svg.append('g').attr('class', 'links').selectAll('path');
    this.linktext = this.svg.append('g').attr('class', 'linktext').selectAll('linktext');
    this.node = this.svg.append('g').attr('class', 'nodes').selectAll('g');

    this.update();
    this.watchRelations();
  }

  dragstarted = () => {
    if (!d3.event.active) { this.simulation.alphaTarget(0.3).restart(); }
    d3.event.subject.fx = d3.event.subject.x;
    d3.event.subject.fy = d3.event.subject.y;
  }
  dragged = () => {
    d3.event.subject.fx = d3.event.x;
    d3.event.subject.fy = d3.event.y;
  }
  dragended = () => {
    if (!d3.event.active) { this.simulation.alphaTarget(0); }
    d3.event.subject.fx = null;
    d3.event.subject.fy = null;
  }
  linkArc(d: any) {
    var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = Math.sqrt(dx * dx + dy * dy);
    return 'M' + d.source.x + ',' + d.source.y + 'A' + dr + ',' + dr + ' 0 0,1 ' + d.target.x + ',' + d.target.y;
  }
  ticked = () => {
    this.link
      .attr('d', this.linkArc);

    this.node.attr('transform', (d: any) => {
      return 'translate(' + d.x + ',' + d.y + ')';
    });

    this.linktext
      .attr('transform', (d: any) => {
        return 'translate(' + (d.source.x + d.target.x) / 2 + ',' + (d.source.y + d.target.y) / 2 + ')';
      });
  }

  update = () => {
    // apply the general update pattern to the nodes.
    this.node = this.node.data(this.model.states, (d) => { return d.id; });
    this.node.exit().remove();
    this.node = this.node.enter()
      .append('g')
      .attr('class', 'node')
      .call(d3.drag()
        .on('start', this.dragstarted)
        .on('drag', this.dragged)
        .on('end', this.dragended))
      .each(function(d: any) {
        d3.select(this).append('circle')
          .attr('r', 10);
        d3.select(this).append('text')
          .attr('dx', 12)
          .attr('dy', '.7em')
          .text((d: any) => { return d.id; });
      })
      .merge(this.node);

    // update 'real world' underline
    this.node.select('text')
      .attr('style', function (d: any) {
        let parent: any = d3.select(this.parentNode);
        if (d.realWorld) {
          parent.attr('style' , 'text-decoration: underline');
        } else {
          parent.attr('style' , 'text-decoration: none');
        }
      });
    this.node.select('circle')
      .attr('class', function (d: any) {
        let parent: any = d3.select(this.parentNode);
        if (d.realWorld) {
          parent.select('circle').attr('style' , 'fill: #f00');
        } else {
          parent.select('circle').attr('style', 'fill: #00f');
        }
      });

    // apply the general update pattern to the links.
    this.link = this.link.data(this.model.relations, (d) => { return d.source.id + '-' + d.target.id; });
    this.link.exit().remove();
    this.link = this.link.enter()
      .append('path')
        .attr('id', (d: any, i: any) => { return 'linkId_' + i; })
        .attr('class', 'link')
        .attr('stroke', ((d: any) => { return this.color(d.agent); }))
      .merge(this.link);

    this.linktext = this.linktext.data(this.model.relations);
    this.linktext.exit().remove();
    this.linktext = this.linktext.enter()
      .append('text')
        .attr('id', (d: any, i: any) => { return 'linktextId_' + i; })
        .attr('class', 'linklabel')
      .merge(this.linktext)
        .text((d: any) => { return _.map(d.agents, (agent: Agent) => { return agent.name; }).join(''); });

    // update and restart the simulation.
    this.simulation.nodes(this.model.states);
    this.simulation.force('link').links(this.model.relations);
    this.simulation.alpha(0.3).restart();
  }

  clean () {
    d3.selectAll('div#graph > *').remove();
  }
}
