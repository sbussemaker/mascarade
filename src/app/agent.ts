import * as _ from 'lodash';

import {Card} from './card';
import * as Strategy from './strategy';
import {GameController} from './game';
import * as config from './config';

export enum Actions {
  SWAP,
  ANNOUNCE,
  PEEK
}

export interface IAgent {
  id: number;
  name: string;
  sayName: () => void;
  setActive: () => void;
  setCard: (card: Card) => void;
  getCard: () => Card;
  getCoins: () => number;
  performTurn: (turnNumber: number) => void;
}

export class Agent implements IAgent {
  public name: string;
  private isActive: boolean;
  private card: Card;
  private action: string;
  private coins: number;
  private swapNextTurn: boolean;

  private strategy: Strategy.Strategy;
  private responseStrategy: Strategy.ResponseStrategies;
  private actionStrategy: Strategy.ActionStrategies;
  private actionStrategyOptions: string[] = Strategy.getActionStrategies();
  private responseStrategyOptions: string[] = Strategy.getResponseStrategies();
  private selectedResponseStrategy: string;
  private selectedActionStrategy: string;

  constructor(
    public id: number,
    public game: GameController,
  ) {
    this.name = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[this.id];
    this.isActive = false;
    this.coins = config.STARTCOINS;
    let n: number = game.nAgents;

    this.selectedActionStrategy = this.actionStrategyOptions[this.id % this.actionStrategyOptions.length];
    this.selectedResponseStrategy = this.responseStrategyOptions[this.id % this.responseStrategyOptions.length];
    this.actionStrategy = Strategy.ActionStrategies[this.selectedActionStrategy];
    this.responseStrategy = Strategy.ResponseStrategies[this.selectedResponseStrategy];
    this.strategy = new Strategy.Strategy(this, Strategy.ActionStrategies[this.selectedActionStrategy],
                                Strategy.ResponseStrategies[this.selectedResponseStrategy]);
  }

  changedStrategy() {
    this.strategy = new Strategy.Strategy(this, Strategy.ActionStrategies[this.selectedActionStrategy],
                                 Strategy.ResponseStrategies[this.selectedResponseStrategy]);
  }

  /** Marks the agent as active, and the others as inactive */
  setActive () {
    _.each(this.game.agents, (agent: Agent) => {
      agent.isActive = false;
    });
    this.isActive = true;
  }

  /** Sets the agent card */
  setCard(card: Card) {
    this.card = card;
  }

  /** do not allow a anouncement or peek next turn */
  forceSwapNextTurn() {
    this.swapNextTurn = true;
  }

  /** Get the agent card */
  getCard() {
    return this.card;
  }

  /** Gets the agent coins */
  getCoins() {
    return this.coins;
  }

  /** Agent introduces itself */
  sayName () {
    console.log(`My name is ${this.name}, my card is ${this.card.peek().name} and I have ${this.coins} coins`);
  }

  /** Choose an action */
  performTurn (turnNumber: number) {
    if (this.swapNextTurn || turnNumber < 4) {
      this.swapNextTurn = false;
      this.action = Actions[Actions.SWAP];
      this.switchCardRandom();
      return;
    }

    let action: Strategy.ActionRet = this.strategy.chooseAction();
    this.action = Actions[action[0]];

    switch (action[0]) {
      case Actions.SWAP:
        this.switchCardRandom();
        break;
      case Actions.ANNOUNCE:
        this.announceCard(action[1]);
        break;
      case Actions.PEEK:
        this.peekCard();
        break;
      default:
        // noop
    }
  }

  /** Perform action 1: Switch card with agent i */
  switchCard(otherAgent: Agent, otherAgents: Agent[]) {
    console.log(`Agent ${this.id} swaps with ${otherAgent.id}`);

    // update game
    let cardCopy: Card = this.getCard();
    this.setCard(otherAgent.getCard());
    otherAgent.setCard(cardCopy);

    this.game.cards = [];
    _.each(this.game.agents, (agent: Agent) => {
      this.game.cards.push(agent.card);
    });

    // update model
    this.game.model.updateSwitched2(this.id, otherAgent.id); // this is interesting!
  }

  /** Perform action 1: Switch card with random agent, other than self */
  switchCardRandom() {
    let otherAgents: Agent[] = _.without(this.game.agents, this.game.agents[this.id]);
    let otherAgent: Agent = otherAgents[Math.floor(Math.random() * otherAgents.length)];
    this.switchCard(otherAgent, otherAgents);
  }

  /** Perform action 2: Announce character */
  announceCard(cardIds: number[] = [-1]) {
    console.log(`My name is ${this.name}, my card is ${this.card.peek().name}`);

    let announcedCard: Card;
    if (cardIds !== [-1]) {
      announcedCard = this.card;
    } else {
      // pick one with equal probability
      let cardId = cardIds[Math.floor(Math.random() * cardIds.length)];
      announcedCard = this.game.cards[cardId];
    }

    let announcingAgents: Agent[] = [this];

    // let the other agents respond to announcement. They can either claim to have the announcedCard as well, or keep silent
    _.each(this.game.agents, (agent: Agent) => {
      if (agent === this) { return; }
      if (agent.respondToAnounce(this, this.card)) {
        announcingAgents.push(agent);
      }
    });

    // if the player next to the current agent responds, force him to swap next round
    let nextAgent: Agent = _.find(announcingAgents, (agent) => { return agent.id === ((this.id + 1)  % this.game.nAgents); });
    if (nextAgent) {
      nextAgent.forceSwapNextTurn();
    }

    if (announcingAgents.length > 1) {
      // determine winner, the agent which actually had the announcedCard. That person gets reward, the rest who announced a penalty
      console.log('Announcing agents: ', announcingAgents);
      _.each(announcingAgents, (agent: Agent) => {
        if (agent.card.id === announcedCard.id) {
          agent.coins += config.REWARD;
        } else {
          agent.coins -= config.PENALTY;
        }
      });
      this.game.model.reveilCards(announcingAgents);
    } else {
      announcingAgents[0].coins += config.REWARD;
    }
  }

  respondToAnounce(anouncingAgent: Agent, anouncedCard: Card) {
    return this.strategy.respondToAnounce(anouncingAgent, anouncedCard);
  }


  /** Perform action 3, peek at own card */
  peekCard() {
    console.log(`My name is ${this.name}, I am peeking my card`);
    this.card.peek();
    // remove all accessibility relations for agent i
    this.game.model.peek(this);
  }
}

export const agent: angular.IComponentOptions = {
  template: require('./agent.html'),
  bindings: {
    agent: '<'
  }
};
