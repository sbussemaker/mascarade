export interface ICard {
  peek: () => any;
}

export class Card implements ICard {

  constructor(
    public name: string,
    public id: number
  ) {}

  peek () {
    return {name: this.name, id: this.id};
  }

  toString () {
    // return this.name;
    return `${this.id} (${this.name})`;
  }
}
