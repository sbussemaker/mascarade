import * as _ from 'lodash';
import * as mathjs from 'mathjs';

import {Agent, Actions} from './agent';
import {Card} from './card';
import {State, Model, Relation} from './model';

export enum ResponseStrategies {
  Credulous, Sceptic, Random,
}

export enum ActionStrategies {
  Bluff, Truthful, Random,
}

export type ActionRet = [Actions, number[]];

export interface IStrategy {
  name: string;
  agent: Agent;
  responseStrategy: ResponseStrategies;
  actionStrategy: ActionStrategies;
  respondToAnounce: (anouncingAgent: Agent, anouncedCard: Card) => boolean;
  chooseAction: () => ActionRet;
}

export class Strategy implements IStrategy {
  public name: string;
  constructor(
    public agent: Agent,
    public actionStrategy: ActionStrategies,
    public responseStrategy: ResponseStrategies,
  ) {; }

  chooseAction() {
    console.log('my action strategy is', this.actionStrategy);
    switch (this.actionStrategy) {
      case ActionStrategies.Bluff:
        return this.chooseActionBluff();
      case ActionStrategies.Truthful:
        let ret: ActionRet = [this.chooseActionTruthful(), [-1]];
        return ret;
      case ActionStrategies.Random:
        ret = this.chooseActionRandom();
        return ret;
      default:
        // noop
    }
  }

  chooseActionRandom() {
    let nAgents: number = this.agent.game.nAgents;
    let possibleCards: number[] = [];
    for (let i: number = 0; i < nAgents; i ++) {
        possibleCards.push(i);
    }
    let ret: ActionRet = [Actions.ANNOUNCE, possibleCards];
    return ret;
  }

  chooseActionTruthful() {
    let model : Model = this.agent.game.model;
    let nAgents: number = this.agent.game.nAgents;
    let agentId: number = this.agent.id;
    let kt: any = model.knowledgeTables[agentId];
    let col: any = mathjs.number(mathjs.subset(kt, mathjs.index(mathjs.range(0, nAgents), agentId)));
    let sum: number = mathjs.sum(col);
    if (sum > 1) {
      if (Math.random() >= 0.5) {
        return Actions.PEEK;
      } else {
        return Actions.SWAP;
      }
    } else {
      return Actions.ANNOUNCE;
    }
  }

  chooseActionBluff() {
    let model : Model = this.agent.game.model;
    let nAgents: number = this.agent.game.nAgents;
    let agentId: number = this.agent.id;
    let kt: any = model.knowledgeTables[agentId];
    let col: any = mathjs.number(mathjs.subset(kt, mathjs.index(mathjs.range(0, nAgents), agentId)));
    let sum: number = mathjs.sum(col);

    let possibleCards: number[] = [];
    for (let i: number = 0; i < nAgents; i ++) {
      if (mathjs.number(mathjs.subset(col, mathjs.index(i, 0))) === 1) {
        possibleCards.push(i);
      }
    }
    let ret: ActionRet = [Actions.ANNOUNCE, possibleCards];
    return ret;

  }

  respondToAnounce(anouncingAgent: Agent, anouncedCard: Card) {
    console.log('my response strategy is', this.responseStrategy);
    switch (this.responseStrategy) {
      case ResponseStrategies.Credulous:
        return this.respondToAnounceCredulous(anouncingAgent, anouncedCard);
      case ResponseStrategies.Random:
        return this.respondToAnounceRandom(anouncingAgent, anouncedCard);
      case ResponseStrategies.Sceptic:
        return this.respondToAnounceSceptic(anouncingAgent, anouncedCard);
      default:
        // noop
    }
  }

  respondToAnounceCredulous(anouncingAgent: Agent, anouncedCard: Card) {
    // "I always believe you""
    return false;
  }

  respondToAnounceRandom(anouncingAgent: Agent, anouncedCard: Card) {
    // "I believe you with P = 1/2"
    if (Math.random() >= 0.5) {
      return false;
    } else {
      return true;
    }
  }

  // challenge when the announcer cannot have the annouced card, but I can.
  respondToAnounceSceptic(anouncingAgent: Agent, anouncedCard: Card) {
    let model : Model = this.agent.game.model;
    let nAgents: number = this.agent.game.nAgents;
    let agentId: number = this.agent.id;
    let kt: any = model.knowledgeTables[agentId];

    if (mathjs.number(mathjs.subset(kt, mathjs.index(anouncedCard.id, anouncingAgent.id))) === 0) {
      if (mathjs.number(mathjs.subset(kt, mathjs.index(anouncedCard.id, agentId))) === 1) {
        return true;
      }
    } else {
      return false;
    }
  }

  toString = (): string => {
    return this.name;
  }
}

export function getResponseStrategies () {
  let strategies = [];
  for (let n in ResponseStrategies) {
    if ( typeof ResponseStrategies[n] === 'number' ) {
      strategies.push(n);
    }
  }
  return strategies;
}

export function getActionStrategies () {
  let strategies = [];
  for (let n in ActionStrategies) {
    if ( typeof ActionStrategies[n] === 'number' ) {
      strategies.push(n);
    }
  }
  return strategies;
}
