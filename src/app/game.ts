import * as angular from 'angular';
import * as _ from 'lodash';
import * as d3 from 'd3';
import * as mathjs from 'mathjs';

import * as config from './config';
import * as Strategy from './strategy';
import {Agent} from './agent';
import {Card} from './card';
import {Model} from './model';

export interface IGameController extends angular.IController {
  nAgents: number;
  agents: Agent[];
  cards: Card[];
  model: Model;
  $scope: angular.IScope;
  initAgents: () => void;
  gameCards: () => Card[];
  announcePlayers: () => void;
  performRound: () => void;
  performTurn: () => void;
}

export class GameController implements IGameController {
  public nAgents: number;
  public nAgentsOptions: number[];
  public agents: Agent[];
  public cards: Card[];

  public model: Model;
  private roundNumber: number;
  private agentTurn: number;
  private turnNumber: number;
  private gameOver: boolean;

  /** @ngInject */
  constructor(public $scope: angular.IScope) {
    this.nAgentsOptions = config.NUM_AGENTS;
    this.nAgents = this.nAgentsOptions[0]; // default with smallest number of agents
    this.setupGame();

    $scope.$watch('$ctrl.nAgents', (oldVal, newVal) => {
      if (newVal === oldVal) { return; }
      this.setupGame();
    });
  }

  /** Sets up the game */
  setupGame () {
    this.gameOver = false;
    this.roundNumber = 1;
    this.agentTurn = 0;
    this.turnNumber = 0;

    this.initAgents();
    this.model = new Model(this.nAgents, this.cards, this);
    this.model.graph.clean();
    this.model.graph.setupModelGraph();
    this.announcePlayers();
  }

  /** Initialize n agents and draw n cards */
  initAgents () {
    this.agents = [];
    this.cards = [];
    let allCards = this.gameCards().slice(0, this.nAgents);
    for (let i = 0; i < this.nAgents; i++) {
      // let pickedCard = allCards.splice(Math.floor(Math.random() * allCards.length), 1)[0];
      let pickedCard = allCards[i];
      this.addAgent(i, pickedCard);
      this.cards = _.concat(this.cards, pickedCard);
    }

    this.agents[0].setActive();
  }

  /** Create agent with some card */
  addAgent (id: number, card: Card) {
    // let name = `Agent ${id + 1}`;
    let newAgent: Agent = new Agent(id, this);
    newAgent.setCard(card);
    this.agents = _.concat(this.agents, newAgent);
  }

  /** Gets the card deck */
  gameCards () {
    let names: string[] = ['Bishop', 'Cheat', 'Fool', 'Inquisitor', 'Judge', 'King', 'Peasant', 'Queen', 'Thief' , 'Widow', 'Witch'];
    let gameCards: Card[] = [];

    for (let i: number = 0; i < names.length; i++) {
      gameCards[i] = new Card(names[i], i);
    }

    return gameCards;
  }

  /** Initialize game model. At this point, everyone sees all the cards */
  initKnowledge() {
    // for each agent
    //   sees current world. Update the model with drawing accessibility relations from the real world to the real world
  }

  /** Lets the agents introduce themselves */
  announcePlayers () {
    _.each(this.agents, (agent: Agent) => {
      agent.sayName();
    });
  }

  /** Perform a complete round, or finish current one */
  performRound () {
    for (let i = this.agentTurn; i < this.nAgents; i++) {
      if (this.gameOver) { return; }
      this.performTurn();
    }
  }

  /** Perform a single turn */
  performTurn () {
    let currentAgent: Agent = this.agents[this.agentTurn];
    currentAgent.setActive();
    currentAgent.performTurn(this.turnNumber);

    // check winning condition
    if (currentAgent.getCoins() >= config.GOALCOINS) {
      this.gameOver = true;
      console.log('Game is over');
    }

    this.agentTurn++; this.turnNumber++;
    this.agents[this.agentTurn % this.nAgents].setActive(); // make next agent active

    if (this.agentTurn >= this.nAgents) {
      this.roundNumber++;
      this.agentTurn = 0;
    }
  }
}

export const game: angular.IComponentOptions = {
  template: require('./game.html'),
  controller: GameController
};
