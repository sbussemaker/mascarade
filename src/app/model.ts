import * as angular from 'angular';
import * as _ from 'lodash';
import * as mathjs from 'mathjs';

import { Card } from './card';
import { Agent } from './agent';
import { IGameController } from './game';
import { Graph } from './graph';
import { Utils } from './utils';
const utils = new Utils;

export class State {
  id: string;
  valuations: Card[];
  realWorld: boolean;
}

export class Relation {
  id: string;
  source: State;
  target: State;
  agents: Agent[];
}

export class Model {
  graph: Graph;
  states: State[];
  relations: Relation[];
  knowledgeTables: any[] = [];
  valuations: number[] = [];

  constructor(private nAgents: number,
              private cards: Card[],
              private game: IGameController,
              private visualization: boolean = true) {
    this.states = [];
    this.relations = [];

    this.graph = new Graph(nAgents, game, this);

    this.initKnowledgeTables();
    if (this.visualization) {
      this.toD3();
    }
  }


 /** Update model after players reveil their cards */
  reveilCards (announcers: Agent[]) {
    for (let i: number = 0; i < announcers.length; i++) {
      let zeroCol: any = mathjs.zeros(this.nAgents, 1);
      let zeroRow: any = mathjs.zeros(1, this.nAgents);
      for (let j: number = 0; j < this.nAgents; j++) {
        this.knowledgeTables[j] = mathjs.subset(this.knowledgeTables[j], mathjs.index(mathjs.range(0, this.nAgents), announcers[i].id), zeroCol);
        this.knowledgeTables[j] = mathjs.subset(this.knowledgeTables[j], mathjs.index(this.valuations[announcers[i].id], mathjs.range(0, this.nAgents)), zeroRow);

        this.knowledgeTables[j] = mathjs.subset(this.knowledgeTables[j], mathjs.index(this.valuations[announcers[i].id], announcers[i].id), 1);
      }
    }

    if (this.visualization) {
      this.toD3();
      this.graph.update();
    }
  }

  /**
   * Update model after a peek, when a agent has peeked his card
   */
  peek (agent: Agent) {
    let zeroCol: any = mathjs.zeros(this.nAgents, 1);
    let zeroRow: any = mathjs.zeros(1, this.nAgents);

    this.knowledgeTables[agent.id] = mathjs.subset(this.knowledgeTables[agent.id], mathjs.index(this.valuations[agent.id], mathjs.range(0, this.nAgents)), zeroRow);
    this.knowledgeTables[agent.id] = mathjs.subset(this.knowledgeTables[agent.id], mathjs.index(mathjs.range(0, this.nAgents), agent.id), zeroCol);
    this.knowledgeTables[agent.id] = mathjs.subset(this.knowledgeTables[agent.id], mathjs.index(this.valuations[agent.id], agent.id), 1);

    if (this.visualization) {
      this.toD3();
      this.graph.update();
    }
  }

  /**
   * Update model after a switch, when two agents switched cards
   */
  updateSwitched2 (agentId: number, otherAgentId: number) {
    // switch the valuations
    let valA: number = this.valuations[agentId];
    let valB: number = this.valuations[otherAgentId];
    this.valuations[agentId] = valB;
    this.valuations[otherAgentId] = valA;

    // update the current player
    let col1 = mathjs.clone(mathjs.subset(this.knowledgeTables[agentId], mathjs.index(mathjs.range(0, this.nAgents), agentId)));
    let col2 = mathjs.clone(mathjs.subset(this.knowledgeTables[agentId], mathjs.index(mathjs.range(0, this.nAgents), otherAgentId)));

    this.knowledgeTables[agentId] = mathjs.subset(this.knowledgeTables[agentId], mathjs.index(mathjs.range(0, this.nAgents), agentId), col2);
    this.knowledgeTables[agentId] = mathjs.subset(this.knowledgeTables[agentId], mathjs.index(mathjs.range(0, this.nAgents), otherAgentId), col1);

    // update the other players
    for (let i: number = 0; i < this.nAgents; i++) {
      if (i !== agentId) {
        let col1 = mathjs.clone(mathjs.subset(this.knowledgeTables[i], mathjs.index(mathjs.range(0, this.nAgents), agentId)));
        let col2 = mathjs.clone(mathjs.subset(this.knowledgeTables[i], mathjs.index(mathjs.range(0, this.nAgents), otherAgentId)));
        let colOr = mathjs.number(mathjs.or(col1, col2));

        this.knowledgeTables[i] = mathjs.subset(this.knowledgeTables[i], mathjs.index(mathjs.range(0, this.nAgents), agentId), colOr);
        this.knowledgeTables[i] = mathjs.subset(this.knowledgeTables[i], mathjs.index(mathjs.range(0, this.nAgents), otherAgentId), colOr);
      }
    }

    if (this.visualization) {
      this.toD3();
      this.graph.update();
    }
  }

  /** register the knowledge at game start, which is the same for each agent
   *  row: cardId
   *  col: playerId
   */
  initKnowledgeTables() {
    let tempTable: any = mathjs.zeros(this.nAgents, this.nAgents);

    // register the knowledge for each agent in the temporary table
    for (let i = 0; i < this.nAgents; i++) {
      tempTable = mathjs.subset(tempTable, mathjs.index(this.cards[i].id, i), 1);
      this.valuations.push(this.cards[i].id);
    }

    // the initital knowledge is the same for each agent at game start, thus we can nAgents times
    // copy the temporary table into each agent's own table.
    for (let i = 0; i < this.nAgents; i++) {
      this.knowledgeTables.push(tempTable.clone());
    }
  }

  /**
   * function that extracts the cards that a player holds possible, for every other player.
   * the function returns a two dimensional array possiblePlayerCards[agent][card], where
   * possiblePlayerCards[agentB] holds an array with all the IDs of cards that the knowledgeTable's agent
   * holds possible for argentB.
   */
  PossiblePlayersCards(knowledgeTable: any) {
    let possiblePlayerCards: number[][] = [];
    for (let agent = 0; agent < this.nAgents; agent++) {
      possiblePlayerCards[agent] = [];
      for (let card = 0; card < this.nAgents; card++) {
        if (mathjs.number(mathjs.subset(knowledgeTable, mathjs.index(card, agent))) === 1) {
        possiblePlayerCards[agent].push(card);
        }
      }
    }
    return possiblePlayerCards;
  }

/**
 * function that recursively traverses the possiblePlayersCards as calculated with PossiblePlayersCards.
 * every possible world/state is returned in an array.
 */
  ValidStates(possiblePlayerCards: number[][], depth: number = 0, growingArray: number[] = [], retArray: State[] = []) {

    // the return section
    // if we have reached the last card in the possible world sequence we should return.
    if (depth >= this.nAgents) {
      let state: State = new State();
      state.id = growingArray.join('');
      if (state.id === this.valuations.join('')) {
        state.realWorld = true;
      }
      state.valuations = [];
      for (let i = 0; i < this.nAgents; i++) {
        state.valuations.push(this.cards[growingArray[i]]);
      }
        retArray.push(state);
    }

    // traverse every possible next leaf.
    let newNodes: number[] = _.without(possiblePlayerCards[depth], ...growingArray);
    for (let i = 0; i < newNodes.length; i++) {
      let arr: number[] = _.clone(growingArray);
      arr.push(newNodes[i]);
      this.ValidStates(possiblePlayerCards, depth + 1, arr, retArray);
    }

    return retArray;
  }

  copyD3properties (oldObj: any, newObj: any) {
    newObj.index = oldObj.index;
    newObj.x = oldObj.x;
    newObj.y = oldObj.y;
    newObj.vx = oldObj.vx;
    newObj.vy = oldObj.vy;
  }

  /** serializes the model to the format required by d3 */
  toD3() {

    // all possible worlds is the union of all worlds holded possible by individual agents.
    let validStates: State[] = [];
    for (let i: number = 0; i < this.nAgents; i++) {
      validStates = _.unionBy(this.ValidStates(this.PossiblePlayersCards(this.knowledgeTables[i])), validStates, (s1) => {return s1.id; });
    }

    let relations: Relation[] = [];
    // get all 2 item combinations of validStates, e.g. [a, b, c] => [[a,b], [a,c], [b,c]]
    let possibleRelations: any[] = utils.k_combinations(validStates, 2);

    // itererate over those combinations, and when ought possible by atleast one player add a relation.
    for (let i: number = 0; i < possibleRelations.length; i++) {
      let possibleRelation1: Relation = new Relation();
      let possibleRelation2: Relation = new Relation();

      possibleRelation1.source = possibleRelations[i][0];
      possibleRelation1.target = possibleRelations[i][1];

      possibleRelation2.source = possibleRelations[i][1];
      possibleRelation2.target = possibleRelations[i][0];

      possibleRelation1.agents = [];
      possibleRelation2.agents = [];

      for (let agent = 0; agent < this.nAgents; agent++) {
        // check if the player holds both the source and target for possible
        let possible: boolean = true;
        for (let playerColumn = 0; playerColumn < this.nAgents; playerColumn++) {
          if (mathjs.number(mathjs.subset(this.knowledgeTables[agent], mathjs.index(possibleRelation1.source.valuations[playerColumn].id, playerColumn))) === 0) {
            possible = false;
          }
          // other way around
          if (mathjs.number(mathjs.subset(this.knowledgeTables[agent], mathjs.index(possibleRelation1.target.valuations[playerColumn].id, playerColumn))) === 0) {
            possible = false;
          }
        }
        if (possible) {
          possibleRelation1.agents.push(this.game.agents[agent]);
          possibleRelation2.agents.push(this.game.agents[agent]);
        }
      }

      if (possibleRelation1.agents.length > 0) {
        let str: string = _.map(possibleRelation1.agents, agent => { return agent.id; }).join('');
        possibleRelation1.id = `A${str}:${possibleRelation1.source.id}t${possibleRelation1.target.id}`;
        possibleRelation2.id = `A${str}:${possibleRelation2.source.id}t${possibleRelation2.target.id}`;
        str = _.map(possibleRelation2.agents, agent => { return agent.id; }).join('');
        relations.push(possibleRelation1, possibleRelation2);
      }
    }

    _.each(relations, (relation: any) => {
      let obj: Relation = _.find(this.relations, { id: relation.id });
      if (obj) {
        this.copyD3properties(obj, relation);
      }
    });
    _.each(validStates, (state: State) => {
      let obj: State = _.find(this.states, { id: state.id });
      if (obj) {
        this.copyD3properties(obj, state);
      }
    });

    this.relations = relations;
    this.states = validStates;
  }
}
