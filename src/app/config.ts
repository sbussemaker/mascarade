export const NUM_AGENTS: number[] = [3, 4, 5, 6, 7, 8, 9];
export const STARTCOINS: number = 6;
export const GOALCOINS: number = 13;
export const REWARD: number = 1;
export const PENALTY: number = 1;
export const NUM_INITIAL_SWAP: number = 4;
