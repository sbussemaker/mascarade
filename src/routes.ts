export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, $locationProvider: angular.ILocationProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      component: 'app'
    })
    .state('theory', {
      url: '/theory',
      template: require('./app/Theory.html')
    })
    .state('strategies', {
      url: '/strategies',
      template: require('./app/CredulousSkeptical.html')
    })
    .state('example', {
      url: '/example',
      template: require('./app/revising.html')
    })
    .state('results', {
      url: '/results',
      template: require('./app/Results.html')
    })
    .state('game', {
      url: '/game',
      component: 'mascaradeGame'
    });
}
