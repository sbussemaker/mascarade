import * as angular from 'angular';

import 'angular-ui-router';
import 'angular-ui-bootstrap';
import routesConfig from './routes';

import {main} from './app/main';
import {agent} from './app/agent';
import {game} from './app/game';
import {Utils} from './app/utils';

import './index.scss';

angular
  .module('app', ['ui.router', 'ui.bootstrap'])
  .config(routesConfig)
  .component('app', main)
  .component('mascaradeAgent', agent)
  .component('mascaradeGame', game)
  .service('Utils', Utils);
